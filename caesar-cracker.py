############Monoalphabetic Substitution############
'''
Description: Caesar/Shift/ROT bruteforcer
Author: Mogons
Usage:
'''

import string
import operator

def remove_ws(strng):
    return strng.translate({ord(c): None for c in string.whitespace})

def wrong():
    print("Wrong Input!")

def exit():
    raise SystemExit(0)

def cracker():

    alphabet = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']

    i = False
    print("Note: No numbers/symbols allowed.\nAll letters will be capitalized.\nWhitespace will be removed.\nType exit to quit the program.")

    while i == False:
        message = input("What would you like to decrypt?").upper()
        message = remove_ws(message)
        print("Input: "+message)

        for character in message:
            if character not in alphabet:
                wrong()
                i = False
                break
            else:
                i = True

        if message == 'EXIT':
            exit()

    text = ""

    with open("Shift_BF-"+message+".txt", "w+") as out:
        for shift in range(1, (len(alphabet))):
            text = ""
            for character in message:
                text += alphabet[operator.sub(alphabet.index(character), shift) % 26]
            out.write("Shift: "+str(shift)+" Plaintext: "+text+"\n")
        print("View all possible solutions: Shift_BF-"+message+".txt")

if __name__=="__main__":
    cracker()
