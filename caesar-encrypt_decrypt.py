############Monoalphabetic Substitution############
'''
Description: Caesar encrypter and decrypter
Author: Mogons
Usage:
'''

import string
import operator

def remove_ws(strng):
    return strng.translate({ord(c): None for c in string.whitespace})

def wrong():
    print("Wrong Input!")

def exit():
    raise SystemExit(0)

def caesar_encode():
    code = "encode"
    sign = "add"
    ciphertext = caesar(code, sign)
    print("Ciphertext: "+ciphertext)

def caesar_decode():
    code = "decode"
    sign = "sub"
    plaintext = caesar(code, sign)
    print("Plaintext: "+plaintext)

def caesar(code, sign):

    alphabet = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
    print("Note: Shifts must be numeric.\nUse a positive number for a right shift and negative for left shift.")
    i = False
    while True:
        try:
            shift = int(input("What shift would you like to use?"))
            break;
        except:
            wrong()
    print("Note: No numbers/symbols allowed.\nAll letters will be capitalized.\nWhitespace will be removed.\nType exit to quit the program.")

    while i == False:
        message = input("What would you like to "+code+"?").upper()
        message = remove_ws(message)
        print("Input: "+message)

        for character in message:
            if character not in alphabet:
                wrong()
                i = False
                break
            else:
                i = True

        if message == 'EXIT':
            exit()

    text = ""

    if sign == "add":
        for character in message:
            text += alphabet[operator.add(alphabet.index(character), shift) % 26]

    elif sign == "sub":
        for character in message:
            text += alphabet[operator.sub(alphabet.index(character), shift) % 26]

    return text



def switch(arg):
    switcher = {
        'encode': caesar_encode,
        'decode': caesar_decode,
        'exit': exit,
     }
    func = switcher.get(arg, lambda: wrong())
    
    return func()

if __name__=="__main__":

    while True:
        task = input("encode/decode/exit?")
        print(task)
        switch(task)

